﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace WCF_Client.Web
{
	public class ServerTimerHub : Hub
	{
		private static ConcurrentDictionary<string, Timer> _timers = new ConcurrentDictionary<string, Timer>();
		private static int _userCount = 0;
		private static int _instanceCount = 0;
		private readonly int _instanceId;
		//---------------------------------------------------------------------
		public ServerTimerHub()
		{
			_instanceId = ++_instanceCount;

			GlobalHost.ConnectionManager.GetHubContext<ServerTimerHub>().Clients.All.Instances(_instanceId, _instanceCount);
		}
		//---------------------------------------------------------------------
		public void StartTimer()
		{
			this.Clients.Caller.InformAboutTransport(this.Context.QueryString["transport"]);
			this.Clients.Caller.InformAboutConnectionId(this.Context.ConnectionId);

			Timer timer = new Timer(_ =>
			{
				string time = string.Format(
					"{0:00}:{1:00}:{2:00}.{3}",
					DateTime.Now.Hour,
					DateTime.Now.Minute,
					DateTime.Now.Second,
					DateTime.Now.Millisecond);

				this.Clients.Caller.SendTime(time, _instanceId, _instanceCount);
			}, null, 500, 100);

			_timers[this.Context.ConnectionId] = timer;
		}
		//---------------------------------------------------------------------
		public void StopTimer()
		{
			Timer timer = null;

			if (!_timers.TryRemove(this.Context.ConnectionId, out timer)) return;

			timer.Dispose();
		}
		//---------------------------------------------------------------------
		public override Task OnConnected()
		{
			Interlocked.Increment(ref _userCount);
			this.Clients.All.Online(_userCount);

			return base.OnConnected();
		}
		//---------------------------------------------------------------------
		public override Task OnDisconnected()
		{
			Interlocked.Decrement(ref _userCount);
			this.Clients.All.Online(_userCount);

			Timer timer;
			_timers.TryRemove(this.Context.ConnectionId, out timer);

			return base.OnDisconnected();
		}
		//---------------------------------------------------------------------
		public override Task OnReconnected()
		{
			Interlocked.Increment(ref _userCount);
			this.Clients.All.Online(_userCount);

			return base.OnReconnected();
		}
	}
}