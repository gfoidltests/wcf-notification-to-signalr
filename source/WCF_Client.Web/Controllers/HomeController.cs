﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;

namespace WCF_Client.Web.Controllers
{
	public class HomeController : Controller
	{
		//
		// GET: /Home/
		public ActionResult Index()
		{
			return this.View();
		}
		//---------------------------------------------------------------------
		// GET: /Home/LongRunning
		public ActionResult LongRunning()
		{
			return this.View(GetLocalIPAddress() as object);
		}
		//---------------------------------------------------------------------
		// GET: /Home/Zeitleiste
		public ActionResult Zeitleiste()
		{
			return this.View();
		}
		//---------------------------------------------------------------------
		private static string GetLocalIPAddress(string hostName = null)
		{
			hostName = hostName ?? Dns.GetHostName();
			List<IPAddress> ips = new List<IPAddress>();

			var host = Dns.GetHostEntry(hostName);
			foreach (var ip in host.AddressList)
				if (ip.AddressFamily == AddressFamily.InterNetwork)
					ips.Add(ip);

			if (ips.Count == 0)
				throw new Exception("Local IP Address Not Found!");

			return string.Join(" | ", ips);
		}
	}
}