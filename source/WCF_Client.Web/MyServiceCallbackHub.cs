﻿using System;
using System.Configuration;
using System.Threading;
using Microsoft.AspNet.SignalR;
using WCF_Proxy;

namespace WCF_Client.Web
{
	public class MyServiceCallbackHub : Hub
	{
		public void DoLongRunningOperation()
		{
			try
			{
				using (ManualResetEventSlim mre = new ManualResetEventSlim())
				using (MyProxy proxy = new MyProxy(ConfigurationManager.AppSettings["ServiceAddress"]))
				{
					proxy.CallbackMessage += msg => this.Clients.Caller.SendMessage(msg);
					proxy.Finished += () =>
					{
						this.Clients.Caller.ServiceFinished();
						mre.Set();
					};

					proxy.DoLongRunningOperation();
					this.Clients.Caller.ServiceStarted();

					mre.Wait();
				}
			}
			catch (Exception ex)
			{
				this.Clients.Caller.OnException(ex.ToString());
			}
		}
	}
}