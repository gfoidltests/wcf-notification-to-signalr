﻿using System;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using WCF_Contracts;

namespace WCF_Service
{
	public class MyService : IMyService
	{
		#region IMyService Members
		public string Hallo(string name)
		{
			return string.Format("Hallo {0}", name);
		}
		//---------------------------------------------------------------------
		public void DoLongRunningOperation()
		{
			TraceHelper.Trace("In Service-method", ConsoleColor.Green);

			string localIP 		 = GetLocalIPAddress();
			IMyCallback callback = OperationContext.Current.GetCallbackChannel<IMyCallback>();

			CallbackData callbackData = new CallbackData();
			callbackData.Message 	  = string.Format("Here ({1}) with T-ID {0:00}", Thread.CurrentThread.ManagedThreadId, localIP);

			callback.LongRunningOperationCallback(callbackData);

			Task.Run(async () =>
			{
				Random rnd = new Random();

				for (int i = 0; i < 10; ++i)
				{
					await Task.Delay(rnd.Next(250, 1000));

					callbackData.Message = string.Format("Here ({2}) with T-ID {0:00} at {1}", Thread.CurrentThread.ManagedThreadId, i + 1, localIP);
					callback.LongRunningOperationCallback(callbackData);
				}

				callback.LongRunningOperationFinished();
			});
		}
		#endregion
		//---------------------------------------------------------------------
		private static string GetLocalIPAddress()
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList)
				if (ip.AddressFamily == AddressFamily.InterNetwork)
					return ip.ToString();

			throw new Exception("Local IP Address Not Found!");
		}
	}
}