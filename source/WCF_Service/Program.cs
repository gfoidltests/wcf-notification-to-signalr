﻿using System;
using System.ServiceModel;
using WCF_Contracts;


namespace WCF_Service
{
	class Program
	{
		static void Main(string[] args)
		{
			using (ServiceHost host = new ServiceHost(
				typeof(MyService),
				new Uri("net.tcp://localhost:8085/myTest")))
			{
				host.Closed 				+= (s, e) => TraceHelper.Trace("Host closed.");
				host.Closing 				+= (s, e) => TraceHelper.Trace("Host closing.");
				host.Faulted 				+= (s, e) => TraceHelper.Trace("Host faulted.");
				host.Opened 				+= (s, e) => TraceHelper.Trace("Host opened.");
				host.Opening 				+= (s, e) => TraceHelper.Trace("Host opening.");
				host.UnknownMessageReceived += (s, e) => TraceHelper.Trace("Unknown message received.");

				host.Open();

				TraceHelper.Trace("Host running. Hit any key to exit...");
				Console.ReadKey();

				host.Close();
			}
		}
	}
}