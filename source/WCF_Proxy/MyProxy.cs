﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCF_Contracts;

namespace WCF_Proxy
{
	public class MyProxy : IMyService, IDisposable
	{
		private interface IMyServiceProxyChannel : IMyService, ICommunicationObject { }
		//---------------------------------------------------------------------
		private class Callback : IMyCallback
		{
			private readonly MyProxy _proxy;
			//---------------------------------------------------------------------
			public Callback(MyProxy proxy)
			{
				_proxy = proxy;
			}
			//---------------------------------------------------------------------
			#region IMyCallback Members
			public void LongRunningOperationCallback(CallbackData callbackData)
			{
				_proxy.OnCallbackMessage(callbackData.Message);
			}
			//---------------------------------------------------------------------
			public void LongRunningOperationFinished()
			{
				_proxy.OnFinished();
			}
			#endregion
		}
		//---------------------------------------------------------------------
		private readonly DuplexChannelFactory<IMyServiceProxyChannel> _myServiceChannel;
		private readonly IMyServiceProxyChannel _proxy;
		//---------------------------------------------------------------------
		public MyProxy(string serviceAddress)
		{
			TraceHelper.Trace("MyProxy Ctor...");

			_myServiceChannel = new DuplexChannelFactory<IMyServiceProxyChannel>(
				new Callback(this),
				new NetTcpBinding { Security = new NetTcpSecurity { Mode = SecurityMode.None } },
				new EndpointAddress(serviceAddress));

			_myServiceChannel.Closed  += (s, e) => TraceHelper.Trace("HelloChannel closed.");
			_myServiceChannel.Closing += (s, e) => TraceHelper.Trace("HelloChannel closing.");
			_myServiceChannel.Faulted += (s, e) => TraceHelper.Trace("HelloChannel faulted.");
			_myServiceChannel.Opened  += (s, e) => TraceHelper.Trace("HelloChannel opened.");
			_myServiceChannel.Opening += (s, e) => TraceHelper.Trace("HelloChannel opening.");

			_myServiceChannel.Open();
			_proxy = _myServiceChannel.CreateChannel();

			_proxy.Closed += (s, e) => TraceHelper.Trace("Proxy closed.");
			_proxy.Closing += (s, e) => TraceHelper.Trace("Proxy closing.");
			_proxy.Faulted += (s, e) => TraceHelper.Trace("Proxy faulted.");
			_proxy.Opened += (s, e) => TraceHelper.Trace("Proxy opened.");
			_proxy.Opening += (s, e) => TraceHelper.Trace("Proxy opening.");

			_proxy.Open();

			TraceHelper.Trace("MyProxy Ctor finished");
		}
		//---------------------------------------------------------------------
		public event Action<string> CallbackMessage;
		private void OnCallbackMessage(string message)
		{
			var tmp = this.CallbackMessage;
			if (tmp != null)
				tmp(message);
		}
		//---------------------------------------------------------------------
		public event Action Finished;
		private void OnFinished()
		{
			var tmp = this.Finished;
			if (tmp != null)
				tmp();
		}
		//---------------------------------------------------------------------
		#region IMyService Members
		public string Hallo(string name)
		{
			return _proxy.Hallo(name);
		}
		//---------------------------------------------------------------------
		public void DoLongRunningOperation()
		{
			_proxy.DoLongRunningOperation();
		}
		#endregion
		//---------------------------------------------------------------------
		#region IDisposable Members
		private bool _isDisposed = false;
		//---------------------------------------------------------------------
		[System.Diagnostics.DebuggerStepThrough]
		protected void ThrowIfDisposed()
		{
			if (_isDisposed) throw new ObjectDisposedException(this.ToString());
		}
		//---------------------------------------------------------------------
		protected virtual void Dispose(bool disposing)
		{
			if (!_isDisposed)
			{
				if (disposing)
				{
					_proxy.Close();
					_myServiceChannel.Close();
				}

				_isDisposed = true;
			}
		}
		//---------------------------------------------------------------------
		[System.Diagnostics.DebuggerStepThrough]
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
}