﻿using System;
using System.Threading;
using WCF_Contracts;
using WCF_Proxy;

namespace WCF_Client.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				System.Console.WriteLine("Waiting for Host to run. Hit any key to continue...");
				System.Console.ReadKey();

				using (ManualResetEventSlim mre = new ManualResetEventSlim())
				using (MyProxy proxy = new MyProxy(Properties.Settings.Default.ServerAddress))
				{
					string hallo = proxy.Hallo("du");
					System.Console.WriteLine(hallo);

					proxy.CallbackMessage += msg => TraceHelper.Trace(msg, ConsoleColor.Yellow);
					proxy.Finished += () => mre.Set();

					proxy.DoLongRunningOperation();

					mre.Wait();
				}
			}
			catch (Exception ex)
			{
				System.Console.WriteLine(ex);
			}

			System.Console.WriteLine("\nEnd.");
			System.Console.ReadKey();
		}
	}
}