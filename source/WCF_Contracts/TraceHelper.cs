﻿using System;
using System.Diagnostics;
using System.Threading;
using Source = System.Diagnostics.Trace;

namespace WCF_Contracts
{
	public static class TraceHelper
	{
		static TraceHelper()
		{
			Source.Listeners.Clear();
			Source.Listeners.Add(new ConsoleTraceListener());
		}
		//---------------------------------------------------------------------
		public static void Trace(string message)
		{
			Source.WriteLine(string.Format(
				"{0}\tT-ID: {1:00}\t{2}",
				DateTime.Now,
				Thread.CurrentThread.ManagedThreadId,
				message));
		}
		//---------------------------------------------------------------------
		public static void Trace(string message, ConsoleColor color)
		{
			ConsoleColor initialColor = Console.ForegroundColor;
			Console.ForegroundColor = color;
			Trace(message);
			Console.ForegroundColor = initialColor;
		}
	}
}