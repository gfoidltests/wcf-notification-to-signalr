﻿using System;
using System.ServiceModel;

namespace WCF_Contracts
{
	[ServiceContract(CallbackContract = typeof(IMyCallback))]
	public interface IMyService
	{
		[OperationContract]
		string Hallo(string name);

		[OperationContract(IsOneWay = true)]
		void DoLongRunningOperation();
	}
	//-------------------------------------------------------------------------
	public class CallbackData
	{
		public string Message { get; set; }
		public Exception Exception { get; set; }
	}
	//-------------------------------------------------------------------------
	public interface IMyCallback
	{
		[OperationContract(IsOneWay = true)]
		void LongRunningOperationCallback(CallbackData callbackData);

		[OperationContract(IsOneWay = true)]
		void LongRunningOperationFinished();
	}
}